package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class WFPServerLogInPage {

  // Defines URL and server constants

  private static final String logInURL = "https://login.workfloplus.com/";
  private static final String localServer = "10.0.1.10";
  private static final String cloudServer = "approvalTest";

  // Stores a copy of the WebDriver passed by argument

  WebDriver webDriver;

  // Page elements of the server login screen

  By serverName = By.id("serverName");
  By submitButton = By.id("btnSubmit");

  // Methods to interact with the page elements

  public WFPServerLogInPage(WebDriver webDriver) {
    this.webDriver = webDriver;
  }

  public void navigateToLogInPage() {
    webDriver.navigate().to(logInURL);
  }

  public void setServerName(String name) {
    webDriver.findElement(serverName).sendKeys(name);
  }

  public void submitServerName() {
    webDriver.findElement(submitButton).click();
  }

  // Functionality methods

  public void logInToServer(String nameOfServer) {
    navigateToLogInPage();
    setServerName(nameOfServer);
    submitServerName();
  }

  public void logInToLocalServer() {
    logInToServer(localServer);
  }

  public void logInToCloudServer() {
    logInToServer(cloudServer);
  }

}
