import PageObjects.WFPServerLogInPage;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class ServerLogInTests {

  WebDriver webDriver;
  WFPServerLogInPage logInPage;

  @BeforeTest
  @Parameters("browser")
  public void testSetup(String browser) throws Exception {
    if (browser.equalsIgnoreCase("chrome")) {
      System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");
      webDriver = new ChromeDriver();
    } else if (browser.equalsIgnoreCase("firefox")) {
      System.setProperty("webdriver.gecko.driver", "drivers/geckodriver");
      webDriver = new FirefoxDriver();
    } else if (browser.equalsIgnoreCase("safari")) {
      webDriver = new SafariDriver();
    } else if (browser.equalsIgnoreCase("edge")) {
      webDriver = new EdgeDriver();
    } else if (browser.equalsIgnoreCase("ie")) {
      webDriver = new InternetExplorerDriver();
    } else {
      throw new Exception("ERROR: Browser not recognised");
    }

    logInPage = new WFPServerLogInPage(webDriver);

  }

  @Test
  public void localServerLogIn() {
    logInPage.navigateToLogInPage();
    logInPage.logInToLocalServer();
  }

  @Test
  public void cloudServerLogIn() {
    logInPage.navigateToLogInPage();
    logInPage.logInToCloudServer();
  }

  @Test
  public void invalidNameServerLogIn() {
    logInPage.navigateToLogInPage();
    logInPage.logInToServer("invalidServerName");
  }

  @Test
  public void invalidLocalServerLogIn() {
    logInPage.navigateToLogInPage();
    logInPage.logInToServer("10.0.1.100");
  }

}
